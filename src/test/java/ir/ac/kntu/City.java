package ir.ac.kntu;

public class City
{
    private String name;
    private double x,y;



    public City(String name,double x, double y)
    {
        this.name=name;
        this.x=x;
        this.y=y;
    }


    public City(String name)
    {
        this.name=name;
        this.x=0;
        this.y=0;
    }



    //getters
    public double getX()
    {
        return this.x;
    }
    public double getY()
    {
        return this.y;
    }
    public String getName()
    {
        return this.name;
    }
    //setters
    public void setX(double x)
    {
       this.x=x;
    }
    public void setY(double y)
    {
        this.y=y;
    }
    public void setName(String name)
    {
        this.name=name;
    }
}
