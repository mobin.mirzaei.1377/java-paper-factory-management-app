package ir.ac.kntu;

public class Customer {
    private String name;
    private int id;

    public Customer(String name,int id)
    {
        this.name=name;
        this.id=id;
    }
    //getter
    public String getName()
    {
        return this.name;
    }
    public int getId()
    {
        return this.id;
    }
    //setter
    public void setName(String name)
    {
        this.name=name;
    }
    public void setId(int id)
    {
        this.id=id;
    }

}
