package ir.ac.kntu;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static ArrayList<Customer> customers = new ArrayList<Customer>();
    public static ArrayList<Package> packages = new ArrayList<Package>();

    public static void main(String[] args)
    {

    }
       public static Customer searchCustomer(String name,int id)
       {
           for (Customer customer : customers)
           {
               if(customer.getName().equals(name) && customer.getId()==id)
               {
                return customer;
               }

           }
           return null;
       }
    public static Package[] packageCity(String name)
    {
        Package[] packs = new Package[packages.size()];
        int i =0;
        for (Package pack : packages)
        {
            if(pack.getStartCity().getName().equals(name) ||
                    pack.getEndCity().getName().equals(name))
            {
              packs[i]=pack;
              i++;
            }

        }
        return packs;
    }
    public static Package[] filterPackageStatus(String status)
    {
        Package[] packs = new Package[packages.size()];
        int i =0;
        for (Package pack : packages)
        {
            if(pack.getStatus().equals(status) )
            {
                packs[i]=pack;
                i++;
            }

        }
        return packs;
    }

    public static void showPackageStatus(Scanner scanner)
    {
        System.out.println("showing all packages ");
        int i=1;
        for (Package pack : packages)
        {
            System.out.println(i + "."+ pack.getName());


        }
        System.out.println("enter package number: ");
        i=scanner.nextInt();
        System.out.println(packages.get(i-1).getStatus());
    }


}
