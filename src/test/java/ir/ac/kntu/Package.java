package ir.ac.kntu;

public class Package
{
   private String name;
   private Customer receiver;
   private Branch branch;
   private City startCity;
   private City endCity;
   private double weight;
   private Date sendDate;
   private Date receiveDate;
   private String method;
   private String type;
   private String status;

    public Package(String name, Customer receiver, Branch branch,
                   City startCity, City endCity, double weight,
                   Date sendDate, Date receiveDate, String method,
                   String type)
    {
        this.name = name;
        this.receiver = receiver;
        this.branch = branch;
        this.startCity = startCity;
        this.endCity = endCity;
        this.weight = weight;
        this.sendDate = sendDate;
        this.receiveDate = receiveDate;
        this.method = method;
        this.type = type;
        this.status = "in stock";
    }



    //getter
    public String getName() {
        return name;
    }

    public Customer getReceiver() {
        return receiver;
    }

    public Branch getBranch() {
        return branch;
    }

    public City getStartCity() {
        return startCity;
    }

    public City getEndCity() {
        return endCity;
    }

    public double getWeight() {
        return weight;
    }

    public Date getSendDate() {
        return sendDate;
    }

    public Date getReceiveDate() {
        return receiveDate;
    }

    public String getMethod() {
        return method;
    }

    public String getType() {
        return type;
    }

    public String getStatus() {
        return status;
    }


    //setter
    public void setName(String name) {
        this.name = name;
    }

    public void setReceiver(Customer receiver) {
        this.receiver = receiver;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }

    public void setStartCity(City startCity) {
        this.startCity = startCity;
    }

    public void setEndCity(City endCity) {
        this.endCity = endCity;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public void setSendDate(Date sendDate) {
        this.sendDate = sendDate;
    }

    public void setReceiveDate(Date receiveDate) {
        this.receiveDate = receiveDate;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setStatus(String status) {
        this.status = status;
    }





       public boolean sent(Date date)
       {
           if(status=="in stock"&& this.sendDate.isLower(date))
           {
               this.status="sent";
               return true;
           }
           else
           {
               return false;
           }
       }




    public boolean received(Date date)
    {
        if(status=="sent"&& this.receiveDate.isGreater(date))
        {
            this.status="received";
            return true;
        }
        else
        {
            return false;
        }
    }



}
