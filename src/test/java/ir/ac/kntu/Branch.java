package ir.ac.kntu;

public class Branch {
    private int id;
    private City city;
    private int empNum;

    public Branch(int id,City city,int empNum)
    {
        this.id=id;
        this.city=city;
        this.empNum=empNum;
    }
    //getter
    public int getId() {
        return id;
    }

    public City getCity() {
        return city;
    }

    public int getEmpNum() {
        return empNum;
    }
    //setter

    public void setId(int id) {
        this.id = id;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public void setEmpNum(int empNum) {
        this.empNum = empNum;
    }
}
